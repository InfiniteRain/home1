
public class Balls {

    enum Color {green, red}

    public static void printBalls(Color[] balls) {
        for (Color ball : balls) {
            System.out.print(ball == Color.red ? "red " : "green ");
        }
        System.out.print("\n");
    }

    public static void main(String[] param) {
        Color[] try1 = new Color[]{Color.green, Color.green, Color.red, Color.red};
        Color[] try2 = new Color[]{Color.green, Color.red, Color.green, Color.red, Color.green, Color.red};
        Color[] try3 = new Color[]{Color.red, Color.red, Color.green, Color.green};

        printBalls(try1);
        reorder(try1);
        printBalls(try1);

        System.out.println("--");

        printBalls(try2);
        reorder(try2);
        printBalls(try2);


        System.out.println("--");

        printBalls(try3);
        reorder(try3);
        printBalls(try3);

        reorder(null);
    }

    public static void reorder(Color[] balls) {
        int lastRedAt = 0;

        for (Color ball : balls) {
            if (ball == Color.red) {
                balls[lastRedAt] = Color.red;
                lastRedAt++;
            }
        }

        for (int i = lastRedAt; i < balls.length; i++) {
            balls[i] = Color.green;
        }
    }
}

